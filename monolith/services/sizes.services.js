const SizeRepository = require('../repository/size.repository')

class SizeService{
    static getAllSize = async () => {
        const sizes = SizeRepository.getAll()
        return sizes
    }
    static searchOne = async (query) => {
        const sizes = SizeRepository.searchOne(query)
        return sizes
    }
}

module.exports = SizeService
