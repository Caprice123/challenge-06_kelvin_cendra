const { checkPassword, createToken, encryptPassword } = require('../helpers/password')
const { NotAuthenticated, NotFound } = require('../helpers/errors')
const { v4: uuidv4 } = require('uuid')
const UserRepository = require('../repository/users.repository')
const RoleRepository = require('../repository/role.repository')

class UserService {
    static signIn = async (email, password) => {
        const user = await UserRepository.searchOne({ email })
        if (!user){
            throw new NotFound()
        }
        
        const authenticate = await checkPassword(user.password, password)
        if (authenticate) {
            const payload = {
                id: user.id,
                username: user.username,
                email: user.email,
                role: user.role.role_name
            }
            const tokenAPI = createToken(payload)
            return { ...payload, token: tokenAPI }
        }
        throw new NotAuthenticated()
    }
    static getAll = async () => {
        const users = await UserRepository.getAll()
        return users
    }


    static getUserById = async (id, role_name) => {
        const role = await RoleRepository.searchOne({ role_name })

        if (!role) {
            throw new NotFound()
        }

        const user = await UserRepository.getOneById( id, role.id )
        
        if (!user){
            throw new NotFound()
        }
        
        return user
    }

    static filterByRole = async (role_name) => {

        const role = await RoleRepository.searchOne({ role_name })

        if (!role) {
            throw new NotFound()
        }
        const allMembers = await UserRepository.filterUser(role.id)
        return allMembers
    }

    static addUser = async (data_user) => {
        const { username, email, password, role_name } = data_user

        const role = await RoleRepository.searchOne({ role_name })

        if (!role) {
            throw new NotFound()
        }
        const id = uuidv4()
        const encryptedPassword = await encryptPassword(password)

        const newUser = await UserRepository.createUser({
            id,
            username,
            email,
            password: encryptedPassword,
            role_id: role.id,
            created_at: new Date(),
            updated_at: new Date()
        })

        return newUser

    }

    static updateUser = async (id, data_user) => {
        const { username, email, password, role_name }  = data_user

        const roleResults = await RoleRepository.searchOne({ role_name })

        if (!roleResults) {
            throw new NotFound()
        }
        const encryptedPassword = await encryptPassword(password)

        const updatedUser = await UserRepository.updateUser(id, {
            username,
            email,
            password: encryptedPassword,
            role_id: roleResults.id,
            updated_at: new Date()
        })

        if (!updatedUser){
            throw new NotFound()
        }
        
        const user = await UserRepository.getOneById(id, roleResults.id)
        return user

    }

    static deleteUser = async (id, role_name) => {
        const role = await RoleRepository.searchOne({ role_name })

        if (!role) {
            throw new NotFound()
        }

        const deletedUser = await UserRepository.deleteUser(id, role.id)
        if (!deletedUser) {
            throw new NotFound()
        }
        return deletedUser
    }

}

module.exports = UserService