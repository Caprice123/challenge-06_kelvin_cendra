const CarRepository = require('../repository/cars.repository')
const { NotFound } = require('../helpers/errors')
const { v4: uuidv4 } = require('uuid')
const SizeRepository = require('../repository/size.repository')


class CarService{
    static getAllCars = async () => {
        const cars = await CarRepository.getAll()
        return cars
    }

    static getCarById = async (id) => {
        const car = await CarRepository.getOneById(id)

        return car
    }

    static searchCar = async (query) => {
        const cars = await CarRepository.filterCar(query)
        return cars
    }

    static createCar = async (data_car, data_user) => {
        const { model, rent_per_day, size, image } = data_car
        const { created_by, updated_by } = data_user
        // generate random id
        const id = uuidv4()

        const sizeCar = await SizeRepository.searchOne({ size_name: size })

        if (!sizeCar){
            throw new NotFound()
        }
        
        const size_code = sizeCar.code

        const newCar = await CarRepository.createCar({
            id,
            model,
            size_code,
            rent_per_day,
            image,
            available_at: '2022-03-23T15:49:05.563Z',
            created_at: new Date(),
            updated_at: new Date(),
            created_by,
            updated_by,
        })
        
        return newCar
    }

    static updateCar = async (id, data_car, data_user) => {
        const { model, rent_per_day, size, image } = data_car
        const { updated_by } = data_user
        const sizeCar = await SizeRepository.searchOne({ size_name: size })

        if (!sizeCar){
            throw new NotFound()
        }
        
        const size_code = sizeCar.code
        const newCar = await CarRepository.updateCar(id, {
            model,
            size_code,
            rent_per_day,
            image,
            updated_at: new Date(),
            updated_by,
        })

        if (!newCar){
            throw new NotFound()
        }

        
        const car = await this.getCarById(id)

        return car
    }

    static deleteCar = async (id, data_user) => {
        const { deleted_by } = data_user

        
        const deletedCar = await CarRepository.updateCar(id, {
            updated_at: new Date(),
            deleted_by
        })
        
        if (!deletedCar){
            throw new NotFound()
        }
        
        const car = await this.getCarById(id)
        
        return car
    }
}


module.exports = CarService
