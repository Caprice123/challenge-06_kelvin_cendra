const fileUpload = require('express-fileupload')
const flash = require('connect-flash')
const session = require('express-session')
const express = require('express')
const dotenv = require('dotenv')
const cors = require('cors')
const ejs = require('ejs')
const swaggerUi = require('swagger-ui-express')
const YAML = require('yamljs')
const swaggerDocument = YAML.load('./swagger.yaml');

const CarApi = require('../api/v1/cars.js')
const SizeApi = require('../api/v1/size.js')
const UserApi = require('../api/v1/user.js')


const CarRoute = require('../routes/cars.js')

const { ERR_400, ERR_401, ERR_404, ERR_409, ERR_500, ERR_403 } = require('../helpers/errors')
dotenv.config()

const app = express()
const PORT = process.env.PORT || 5000

app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use(cors())
app.use(flash())
app.set('trust proxy', 1)
app.use(session({
    secret: "123",
    saveUninitialized: false,
    resave: false
}))
app.use(fileUpload());
app.use(express.static('./public'));

app.engine('html', ejs.render)
app.set("view engine", 'html')

app.use('/cars', CarRoute)
app.use('/api/v1/cars', CarApi)
app.use('/api/v1/size', SizeApi)
app.use('/api/v1/users', UserApi)
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use((error, req, res, next) => {
    console.log(error.message)
    switch(error.message){
        case ERR_400:
            res.status(400)
            break
        case ERR_401:
            res.status(401)
            break
        case ERR_403:
            res.status(403)
            break
        case ERR_404:
            res.status(404)
            break
        case ERR_409:
            res.status(409)
            break
        case ERR_500:
        default:
            res.status(500)
            break
    }
    res.send({ success: false, results: [] })
})

app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`)
})
