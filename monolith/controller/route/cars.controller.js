const { deleteData, fetchData, postData, putData, fetchDataOne } = require('../../helpers/fetch.js')
const { renderView } = require('../../helpers/view.js')
const { removeFile, saveFile, checkMultipleFile } = require('../../helpers/file.js')
const dotenv = require('dotenv')

dotenv.config()
const HOSTNAME = process.env.HOSTNAME || 'http://127.0.0.1:5000'

class CarRoute{
    static handleListCar = async (req, res, next) => {
        // parsing the flash message
        const [add = null] = req.flash("add")
        const [del = null] = req.flash("del")

        try{
            // get all available Size from size API
            const { results: availableSizes } = await fetchData({
                url: `${HOSTNAME}/api/v1/size`
            })
            
            // render view
            const view = {
                path: 'carsList.html',
                args: {
                    user: "Unis Badri",
                    availableSizes,
                    add,
                    del
                }
            }
            renderView(res, view.path, view.args)
        } catch(err){
            next(err)
        }
    }

    static handleAddCarPage = async (req, res, next) => {
        try{
            // get all available Size from size API
            const { results: availableSizes } = await fetchData({
                url: `${HOSTNAME}/api/v1/size`
            })

            // render view
            const view = {
                path: 'carsAdd.html',
                args: {
                    user: "Unis Badri",
                    add: true,
                    initialData: null,
                    availableSizes
                }
            }
            renderView(res, view.path, view.args)
        } catch(err){
            next(err)
        }
    }

    static handleUpdateCarPage = async (req, res, next) => {        
        try{
            // get original data that will be updated for the initial data
            const { results } = await fetchDataOne({
                url: `${HOSTNAME}/api/v1/cars/${req.params.id}`
            })

            // get all available size from size API
            const { results: availableSizes } = await fetchData({
                url: `${HOSTNAME}/api/v1/size`
            })

            // render view
            const view = {
                path: 'carsUpdate.html',
                args: {
                    user: "Unis Badri",
                    add: false,
                    initialData: results,
                    availableSizes
                }
            }
            renderView(res, view.path, view.args)
            
            
        } catch(err){
            next(err)
        }
    }

    
    static handleAddCar = async (req, res, next) => {
        // parsing data from input text and file from input file
        const { name, rent, size } = req.body
        const { image } = req.files

        try{
            const imageName = checkMultipleFile(image.name)
            // posting data to save to the database
            await postData({
                url: `${HOSTNAME}/api/v1/cars`,
                payload: {
                    model: name,
                    rent_per_day: Number(rent.replaceAll("Rp.", "").replaceAll(",", "").trim()),
                    size,
                    image: imageName
                }
            })
            // saving the file
            saveFile(image, imageName)

            // back to list car
            req.flash("add", true)
            res.redirect('/cars/list')
        } catch(err){
            next(err)
        }

    }

    static handleDeleteCar = async (req, res, next) => {
        // get the id that will be deleted from url
        const { id } = req.params

        try{
            // get the original data
            const { results } = await fetchData({
                url: `${HOSTNAME}/api/v1/cars`,
                params: req.params
            })

            // deleting data in database
            await deleteData({
                url: `${HOSTNAME}/api/v1/cars/${id}`,
            })
            
            // removing the file
            removeFile(results[0].image)

            // back to page list car
            req.flash("del", true)
            res.redirect('/cars/list')

        } catch (err){
            next(err)
        }
    }
    
    static handleUpdateCar = async (req, res, next) => {
        // parsing data from input text and file from input file
        const { image } = req.files
        const { name, rent, size } = req.body
        const { id } = req.params

        try{
            // get the original data
            const { results } = await fetchDataOne({
                url: `${HOSTNAME}/api/v1/cars/${id}`
            })

            
            const imageName = checkMultipleFile(image.name)
            // updating data
            await putData({
                url: `${HOSTNAME}/api/v1/cars/${id}`,
                payload: {
                    model: name.trim(),
                    rent_per_day: Number(rent.replaceAll("Rp.", "").replaceAll(",", "").trim()),
                    size,
                    image: imageName
                }
            })

            // remove the file if the new image is not the same as the original image
            let originalImage = results[0].image
            originalImage = originalImage.split("/")[originalImage.split("/").length - 1]
    
            if (originalImage != image.name){
                removeFile(results[0].image)
                saveFile(image, imageName)
            }
            
            // redirect to the list car page
            req.flash("add", true)
            res.redirect("/cars/list")
            
        } catch(err){
            next(err)
        }
    }
}

module.exports =  CarRoute
