
const { InternalServerError } = require('../../helpers/errors.js')
const SizeService = require('../../services/sizes.services.js')


class SizeApiController {
    static handleGet = async (req, res, next) => {
        
        // cleaning the query for the database
        try{
            let results
            if (Object.keys(req.query).length === 0){
                results = await SizeService.getAllSize()
            } else {
                results = await SizeService.searchOne(req.query)
            }
            res.status(200).json({ success: true, results })
            
        } catch(err){
            next(err)
        }
        
    }

}
module.exports =  SizeApiController
