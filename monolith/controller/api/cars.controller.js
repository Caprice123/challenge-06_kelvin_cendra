const CarService = require('../../services/cars.services');

class CarsApiController {
    static handleGet = async (req, res, next) => {
        try{
            let results
            if (Object.keys(req.query).length === 0){
                results = await CarService.getAllCars()
            } else {
                const { size_name = 'all', model = '' } = req.query
                results = await CarService.searchCar({ size_name, model })
            }
            res.status(200).json({ success: true, results })
        } catch(err){
            next(err)
        }
    }


    static handleGetOne = async (req, res, next) => {        
        try{
            const car = await CarService.getCarById(req.params.id)
            res.status(200).json({ success: true, results: car })
        } catch(err){
            next(err)
        }
    }

    static handlePost = async (req, res, next) => {
       try{
             // parsing data from request body
             const { model, rent_per_day, size, image } = req.body
             const { username } = req.user
 
             const newCar = await CarService.createCar({ model, rent_per_day, size, image}, { created_by: username, updated_by: username })
             res.status(201).json({ success: true, results: newCar })
            
        } catch(err){
            next(err)
        }
    }
    static handlePut = async (req, res, next) => {
        try{
            // parsing data from request body
            const { id } = req.params
            const { model, rent_per_day, image, size } = req.body
            const { username } = req.user
        

            const updatedCar = await CarService.updateCar(id, { model, rent_per_day, image, size}, { updated_by: username })
            res.status(200).json({ success: true, results: updatedCar })
            
        } catch(err){
            next(err)
        }
    }

    static handleDelete = async (req, res, next) => {
        try{
            const { username } = req.user
        
            const deletedCar = await CarService.deleteCar(req.params.id, { deleted_by: username })
            res.status(200).json({ success: true, results: deletedCar })
        } catch (err){
            next(err)
        }
    }
}


module.exports = CarsApiController