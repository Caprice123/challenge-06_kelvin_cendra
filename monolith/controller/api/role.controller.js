const RoleService = require('../../services/roles.services')
const { InternalServerError } = require('../../helpers/errors.js')


class RoleApiController {
    static handleGet = async (req, res, next) => {
        
        // cleaning the query for the database
        try{
            let results
            if (Object.keys(req.query).length === 0){
                results = await RoleService.getAllSize()
            } else {
                results = await RoleService.searchOne(req.query)
            }
            res.status(200).json({ success: true, results })
            
        } catch(err){
            next(err)
        }
        
    }

}
module.exports =  RoleApiController
