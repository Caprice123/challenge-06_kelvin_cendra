class ListCar{
    static list = []
    static init(cars){
        this.list = cars.map(car => new this(car))
    }

    constructor({
        id,
        image,
        model,
        rent_per_day,
        available_at
    }){
        this.id = id
        this.image = image
        this.model = model
        this.rent = rent_per_day
        this.available_at = available_at
    }

    render = () => {
        return (
            `
            <div class="card py-3 pt-5">
                <img src="${this.image}" class="card-img-top mx-auto" alt="...">
                <div class="card-body d-flex flex-column justify-content-between mx-auto  mt-2 pt-5">
                    <p class="card-text">${this.model}</p>
                    <h5 class="card-title">Rp ${Number(this.rent).toLocaleString()} / hari</h5>
                    <p class="card-text">
                        <span><i class="fa-solid fa-clock"></i></span>
                        <span>${new Date(this.available_at)}</span>
                    </p>
                    <div class="group-buttons d-flex justify-content-between">
                        <a type="button" class="btn btn-outline-danger border-2 delete-button" data-id=${this.id} data-bs-toggle="modal" data-bs-target="#exampleModal">
                            <i class="fa-solid fa-trash"></i>
                            <span class="ms-2" >Delete</span>
                        </a>
                        <a href="/cars/list/update/${this.id}" type="button" class="btn btn-success border-2">
                            <i class="fa-solid fa-pen-to-square"></i>
                            <span class="ms-2">Edit</span>
                        </a>
                    </div>
                </div>
            </div>
            `
        )
    }
}