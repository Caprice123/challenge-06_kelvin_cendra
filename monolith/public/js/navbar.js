class Navbar{
    constructor(){
        this.icon = document.querySelector("#navbar-icon")
        this.sidebarNavbar = document.querySelector("#sidebar-navbar")
        this.content = document.querySelector("#content")
        this.navbar = document.querySelector(".navbar")
    }

    init(){
        this.icon.onclick = this.toggleSidebar
    }

    toggleSidebar = () => {
        if (!this.icon.classList.contains("fa-xmark")){
            this.icon.classList = "fa-solid fa-xmark ms-4"
            this.sidebarNavbar.classList.add("active")
            this.navbar.classList.add("active")
            this.content.classList.add("navbar-active")
        } else {
            this.icon.classList = "fa-solid fa-bars ms-4"
            this.sidebarNavbar.classList.remove("active")
            this.navbar.classList.remove("active")
            this.content.classList.remove("navbar-active")
        }
    }
}

