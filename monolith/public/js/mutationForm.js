class MutationForm{
    #inputName
    #inputRent
    #inputImage
    #formImage
    #inputButton
    #saveButton

    constructor(){
        this.#inputName = document.querySelector("input.name")
        this.#inputRent = document.querySelector("input.rent")
        this.#inputImage = document.querySelector("input.imageName")
        this.#formImage = document.querySelector("input.image")
        this.#inputButton = document.querySelector(".submit-button")
        this.#saveButton = document.querySelector('.save-button')
    }


    async init(){
        if (this.#inputImage.value !== ''){
            const [buffer, MIME] = await this.#fetchImage(this.#inputImage.value)
            let fileName = this.#inputImage.value
            fileName = fileName.split("/")[fileName.split("/").length - 1]
            this.#initializeInputImage(buffer, fileName, MIME)
        }


        this.#inputName.onkeydown = this.#validateName
        this.#inputRent.onkeyup = this.#validateRent
        this.#inputImage.onclick = this.#openFormImage
        this.#formImage.onchange = this.#validateImage
        this.#saveButton.onclick = this.#validateData
        
    }

    #initializeInputImage = (buffer, filename, MIME) => {
        let blob = new File([buffer], filename, { type: MIME })
        let container = new DataTransfer()
        container.items.add(blob)
        
        this.#formImage.files = container.files
    }
    #fetchImage = async (url) => {
        try{
            const response = await axios.get(url, {
                responseType: 'arraybuffer'
            })
            return [response.data, response.headers['content-type']]
        } catch(err){
            this.#formImage.value = null
            this.#inputImage.value = null
            return []
        }
        
    }

    #validateName = (e) => {
        this.#reset()
        var regex = new RegExp('[a-z ]', 'i') 

        if (!e.key.match(regex) || (this.#inputName.value.length === 0 && e.key === ' ')){
            e.preventDefault()
            return
        }
        
        if (this.#inputName.value.charAt(this.#inputName.value.length - 1) === " " && e.key === " "){
            e.preventDefault()
            return
        }
    }

    #validateRent = (e) => {
        this.#reset()
        var regex = new RegExp('[0-9]')
        if (!e.key.match(regex)){
            this.#inputRent.value = this.#inputRent.value.slice(0, -1)
            e.preventDefault()
            return
        }

        this.#inputRent.value = this.#inputRent.value.replaceAll('Rp.', '').replaceAll(",", '').trim()
        this.#inputRent.value = `Rp. ${Number(this.#inputRent.value).toLocaleString()}`
        
    }

    #openFormImage = () => {
        this.#formImage.click()
    }

    #validateImage = (e) => {
        this.#reset()
        const file = this.#formImage.files[0]
        const size = file.size
        if (size / 1024 > 2048){
            this.#formImage.value = ''
            this.#inputImage.value = ''
            e.preventDefault()
            return
        }
        
        this.#inputImage.value = file.name
    }

    #validateData = (e) => {
        const name = this.#inputName.value.trim()
        const rent = this.#inputRent.value.replaceAll("Rp.", '').replaceAll(",", '').trim()
        const image = this.#formImage.files
        let error = false
        
        if (name.length === 0){
            this.#reset()
            setTimeout(() => {
                this.#inputName.classList.add("invalid")
            }, 0)
            error = true
        }
        if (rent.length === 0){
            this.#reset()
            setTimeout(() => {
                this.#inputRent.classList.add("invalid")
            }, 0)
            error = true
        }
        if (image.length === 0){
            this.#reset()
            setTimeout(() => {
                this.#inputImage.classList.add("invalid")
            }, 0);
            error = true
        }

        if (error){
            e.preventDefault()
        } else{
            this.#inputButton.click()
        }
        
    }


    #reset = () => {
        this.#inputName.classList.remove("invalid")
        this.#inputRent.classList.remove("invalid")
        this.#inputImage.classList.remove("invalid")
    }
}
new MutationForm().init()