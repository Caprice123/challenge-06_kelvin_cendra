const { Router } = require('express')
const CarRoute = require('../controller/route/cars.controller.js')

const router = Router()

router.get('/list', CarRoute.handleListCar)

router.get('/list/add', CarRoute.handleAddCarPage)
        .post('/list/add', CarRoute.handleAddCar)

router.get('/list/update/:id', CarRoute.handleUpdateCarPage)
        .post('/list/update/:id', CarRoute.handleUpdateCar)

router.get('/list/delete/:id', CarRoute.handleDeleteCar)

module.exports = router
