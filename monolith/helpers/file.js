
const { unlink } = require('fs')
const fs = require('fs')

const saveFile = (file, filename) => {
    try{
        file.mv(`./public${filename}`, (err) => {
            if (err){
                throw new Error(err)
            }
        })
    } catch(err){
        throw new Error(err.message)
    }   
}

const removeFile = (filename) => {
    unlink(`./public${filename}`, err => {
        if (err){
            throw new Error(err)
        } 
    })
}

const checkFile = (filename) => {
    try{
        if (fs.existsSync(`./public/uploads/${filename}`)){
            return true
        }
        return false
    } catch(err){
        throw new Error(err.message)
    }
}

const checkMultipleFile = (filename) => {
    let [imageName, extension] = filename.split(".")
            
    imageName = imageName.replace("/uploads/", "")
    if (checkFile(`${imageName}.${extension}`)){
        let i = 1
        while(checkFile(`${imageName} (${i}).${extension}`)){
            i += 1;
        }
        imageName = `/uploads/${imageName} (${i}).${extension}`
    } else{
        imageName = `/uploads/${imageName}.${extension}`
    }
    console.log(imageName)
    return imageName
}

module.exports = { saveFile, removeFile, checkMultipleFile }
