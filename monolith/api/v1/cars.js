const { Router } = require('express')
const Authentication = require('../../middleware/authentication')
const CarApiController = require('../../controller/api/cars.controller.js')

const router = Router()

router.get('/', CarApiController.handleGet)
        .get('/:id', CarApiController.handleGetOne)
        .post('/',[
                Authentication.tokenRequired,
                Authentication.privateAPI
        ], CarApiController.handlePost)
        .put('/:id',[
                Authentication.tokenRequired,
                Authentication.privateAPI
        ], CarApiController.handlePut)
        .delete('/:id',[
                Authentication.tokenRequired,
                Authentication.privateAPI
        ], CarApiController.handleDelete)

module.exports = router

