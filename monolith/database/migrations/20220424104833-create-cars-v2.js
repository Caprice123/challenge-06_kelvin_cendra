'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.addColumn(
        'cars',
        'created_by',
        {
          type: Sequelize.STRING,
          allowNull: false,
        }
      ),
      
      queryInterface.addColumn(
        'cars',
        'updated_by',
        {
          type: Sequelize.STRING,
          allowNull: false,
        },
      ),
      queryInterface.addColumn(
        'cars',
        'deleted_by',
        {
          type: Sequelize.STRING,
        }
      ),

    ]);

  },
  async down(queryInterface, Sequelize) {
    Promise.all([
      queryInterface.removeColumn('cars', 'created_by'),
      queryInterface.removeColumn('cars', 'deleted_by'),
      queryInterface.removeColumn('cars', 'updated_by')
    ]);
  }
};