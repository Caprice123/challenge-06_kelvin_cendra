'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('roles', [
          {
            id: 1,
            role_name: 'superadmin',
            created_at: new Date(),
            updated_at: new Date()
          }, {
            id: 2,
            role_name: 'admin',
            created_at: new Date(),
            updated_at: new Date()
          }, {
            id: 3,
            role_name: 'member',
            created_at: new Date(),
            updated_at: new Date()
          }
      ], {});
  },

  async down (queryInterface, Sequelize) {
     await queryInterface.bulkDelete('roles', null, {});
  }
};
