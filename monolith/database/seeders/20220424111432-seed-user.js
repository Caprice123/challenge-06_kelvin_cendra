'use strict';
const { encryptPassword } = require('../../helpers/password')
const { v4: uuidv4 } = require('uuid')

module.exports = {
  async up (queryInterface, Sequelize) {
    const superuser_password = await encryptPassword("superadmin123")
    await queryInterface.bulkInsert('users', [
      {
        id: uuidv4(),
        username: "superadmin",
        email: "superadmin@gmail.com",
        password: superuser_password,
        role_id: 1,
        created_at: new Date(),
        updated_at: new Date()
      },
  ], {});

  },

  async down (queryInterface, Sequelize) {
     await queryInterface.bulkDelete('users', null, {});
  }
};
