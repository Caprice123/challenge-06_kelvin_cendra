# Car Management Dashboard
A monolith car management dashboard is a system where a user with a role superadmin and admin can do CRUD stuff about car data. This system divides user into 3 roles:

- Member: Can see cars, can see car sizes

- Admin: Can do CRUD about car stuff

- Superadmin: Can do CRUD about car stuff and admin stuff.

For starter user, there is superadmin with credentials:

email: superadmin@gmail.com

password: superadmin123

For documentation api, the link is:
http://127.0.0.1:5000/api-docs

Technologies used: Express, Node.js, PostgreSQL, Swagger UI

## How to run
```bash
npm install
npm run create-database
npm run migrate
npm run seed
npm start 
```

### Endpoints REST API

GET /api/v1/users -> Getting all users

POST /api/v1/users/login -> Logins user

GET /api/v1/users/logout -> Logout user

GET /api/v1/users/profile -> Get current user description

GET /api/v1/users/admins -> Get all admins

POST /api/v1/users/admins -> Create new admin

GET /api/v1/users/admins/:id -> Get a specific admin by ID

PUT /api/v1/users/admins/:id -> Update a specific admin by ID

DELETE /api/v1/users/admins/:id -> Delete a specific admin by ID

GET /api/v1/users/members -> Get all members

POST /api/v1/users/members -> Create new member

GET /api/v1/users/members/:id -> Get a specific member by ID

PUT /api/v1/users/members/:id -> Update a specific member by ID

DELETE /api/v1/users/members/:id -> Delete a specific member by ID

GET /api/v1/cars -> Getting all cars

GET /api/v1/cars/:id -> Getting a specific car by id

POST /api/v1/cars -> Creating a car

PUT /api/v1/cars -> Update a car

DELETE /api/v1/cars/:id -> Deleting a specific car by id

GET /api/v1/size -> Getting all available car size

## Directory Structure

```
.
├── api
│   └── v1
│       ├── cars.js
│       ├── size.js
│       └── user.js
├── controller
│   ├── api
│   │   ├── cars.controller.js
│   │   ├── role.controller.js
│   │   ├── size.controller.js
│   │   └── user.controller.js
│   └── route   
│       └── cars.controller.js
├── database
│   ├── config
│   │   └── config.json
│   ├── migrations
│   │   ├── 20220417115219-create-size.js
│   │   ├── 20220417115258-create-cars.js
│   │   ├── 20220424104831-create-role.js
│   │   ├── 20220424104832-create-user.js
│   │   └── 20220424104833-create-cars-v2.js
│   ├── models
│   │   ├── cars.js
│   │   ├── index.js
│   │   ├── role.js
│   │   ├── size.js
│   │   └── user.js
│   └── seeders
│       ├── 20220417094652-seed-size.js
│       ├── 20220417094652-seed-cars.js
│       ├── 20220424111425-seed-role.js
│       └── 20220424111432-seed-user.js
├── helpers
│   ├── errors.js
│   ├── fetch.js
│   ├── file.js
│   ├── password.js
│   └── view.js
├── middleware
│   └── authenticate.js
├── public
│   ├── css
│   │   ├── actionButton.css
│   │   ├── dashboard.css
│   │   ├── flash.css
│   │   ├── main.css
│   │   ├── mutationForm.css
│   │   ├── navbar.css
│   │   ├── popup.css
│   │   ├── sidebar.css
│   │   └── sidebarNavbar.css
│   ├── images
│   │   ├── imagebeepbeep.svg
│   │   └── image_car.css
│   ├── js
│   │   ├── dashboard.js
│   │   ├── delete.js
│   │   ├── filter.js
│   │   ├── listCar.js
│   │   ├── mutationForm.js
│   │   └── navbar.js
│   └── uploads
├── repository
│   ├── cars.repository.js
│   ├── role.repository.js
│   ├── size.repository.js
│   └── users.repository.js
├── routes
│   └── cars.js
├── server
│   └── index.js
├── services
│   ├── cars.services.js
│   ├── role.services.js
│   └── size.services.js
├── views
│   ├── partials
│   │   ├── actionButtons.html
│   │   ├── cardCollection.html
│   │   ├── filter.html
│   │   ├── flash.html
│   │   ├── location.html
│   │   ├── mutationForm.html
│   │   ├── navbar.html
│   │   ├── popup.html
│   │   ├── sidebar.html
│   │   └── sidebarNavbar.html
│   ├── carsAdd.html
│   ├── carsList.html
│   ├── carsUpdate.html
│   └── templates.html
├── .sequelizerc
├── package.json
├── README.md
└── swagger.yaml
```

## ERD
![Entity Relationship Diagram](ERD_diagram.png)

