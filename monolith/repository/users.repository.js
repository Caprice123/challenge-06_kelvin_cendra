const { user: Users, role: Roles } = require('../database/models')
const { BadRequest, ResourceConflict, InternalServerError } =  require('../helpers/errors')

class UserRepository{
    static getAll = async () => {
        try{
            const users = await Users.findAll({
                include: [{
                    model: Roles,
                    attributes: ['role_name']
                }]
            })
            return users
        } catch(err){
            throw new InternalServerError()
        }
        
    }

    static searchOne = async (query) => {
        let users
        try{
            const { email } = query   
            users = await Users.findOne({
                where: { email },
                include: [{
                    model: Roles,
                    attributes: ['role_name']
                }]
            })
            return users
            
        } catch(err){
            console.log(err.message)
            throw new InternalServerError()
        }

    }

    static getOneById = async (id, role_id) => {
        try{
            console.log(id, role_id)
            const users = await Users.findOne({
                where: { id, role_id },
                include: [{
                    model: Roles,
                    attributes: ['role_name']
                }]
            })
            console.log(users)
            return users
        } catch(err){
            throw new InternalServerError()
        }
    }

    static filterUser = async (role_id) => {
        try{
            const users = await Users.findAll({
                where: { role_id },
                include: [{
                    model: Roles,
                    attributes: ['role_name']
                }]
            })
            return users
        } catch(err){
            throw new InternalServerError()
        }
    }

    static createUser = async (data_user) => {
        try{
            const newUser = await Users.create(data_user)
            return newUser
           
        } catch(err){
            if (err.message === "Validation error"){
                throw new ResourceConflict()
            }
            throw new BadRequest()
        }
    }

    static updateUser = async (id, data_user) => {
        try{
            const updatedUser = await Users.update(data_user, { where: { id } })
            return updatedUser
        } catch(err){
            if (err.message === "Validation error"){
                throw new ResourceConflict()
            }
            throw new BadRequest()
        }
    }

    static deleteUser = async (id, role_id) => {
        try{
            const deletedUser = await Users.destroy({ where: { id, role_id }})
            return deletedUser
        } catch(err){
            throw new InternalServerError()
        }
    }

}

module.exports = UserRepository