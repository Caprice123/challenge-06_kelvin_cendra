const { cars: Cars, size: Size } = require('../database/models')
const { BadRequest, ResourceConflict, InternalServerError } =  require('../helpers/errors')


class CarRepository{
    static getAll = async () => {
        try{
            const cars = await Cars.findAll({
                include: [
                    {
                        model: Size,
                        attributes: ['size_name']
                    }
                ],
                where: { deleted_by: null }
            })
            return cars
        } catch(err){
            throw new InternalServerError()
        }
        
    }

    static getOneById = async (id) => {
        try{
            const cars = await Cars.findOne({
                where: { id, deleted_by: null },
                include: [
                    {
                        model: Size,
                        attributes: ['size_name']
                    }
                ]
            })
            // console.log(cars)
            return cars
        } catch(err){
            throw new InternalServerError()
        }
    }

    static filterCar = async (query) => {
        try{
            const { size_name, model } = query
            const query_param = {
                where: { deleted_by: null },
                include: [{
                        model: Size,
                        attributes: ['size_name'],
                    }
                ]
            }
    
            if (size_name && size_name !== 'all'){
                query_param.include[0].where = { size_name: size_name.charAt(0).toUpperCase() + size_name.slice(1) }
            }
    
            if (model){
                query_param.where = { ...query_param.where, model }
            }
    
            const cars = await Cars.findAll(query_param)
            return cars
        } catch(err){
            throw new InternalServerError()
        }
    }

    static createCar = async (data_car) => {
        try{
            const newCar = await Cars.create(data_car)
            return newCar
        } catch(err){
            console.log(err)
            throw new BadRequest()
        }
    }

    static updateCar = async(id, data_car) => {
        try{
            const updatedCar = await Cars.update(data_car, { where: { id } })
            return updatedCar
            
        } catch(err){
            throw new BadRequest()
        }
    }

    static deleteCar = async (id) => {
        try{
            const deletedCar = await Cars.destroy({
                where: { id }
            })
            return deletedCar
        } catch(err){
            throw new InternalServerError()
        }
    }
}

module.exports = CarRepository
