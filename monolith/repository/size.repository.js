const { size: Size } = require('../database/models/index.js')
const { InternalServerError } = require('../helpers/errors.js')

class SizeRepository {
    static searchOne = async (query) => {
        
        // cleaning the query for the database
        try{
            const { size_name } = query
            // querying from the database
            const sizes =  await Size.findOne({
                where: { size_name }
            })
            return sizes
            
        } catch(err){
            throw new InternalServerError()
        }
        
    }

    static getAll = async () => {
        // cleaning the query for the database
        try{
            
            // querying from the database
            const sizes =  await Size.findAll()
            return sizes
            
        } catch(err){
            throw new InternalServerError()
        }
        
    }

}
module.exports =  SizeRepository