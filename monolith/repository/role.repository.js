const { role: Role } = require('../database/models/index.js')
const { InternalServerError } = require('../helpers/errors.js')

class RoleRepository {
    static searchOne = async (query) => {
        // cleaning the query for the database
        try{
            const { role_name } = query
            // querying from the database
            const roles =  await Role.findOne({
                where: { role_name }
            })
            return roles
            
        } catch(err){
            throw new InternalServerError()
        }
        
    }


    static getAll = async () => {
        // cleaning the query for the database
        try{
            
            // querying from the database
            const roles =  await Role.findAll()
            return roles
            
        } catch(err){
            throw new InternalServerError()
        }
        
    }

}
module.exports =  RoleRepository