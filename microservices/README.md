# Car Management Dashboard
A microservice car management dashboard is a system where a user with a role superadmin and admin can do CRUD stuff about car data. This system divides user into 3 roles:

- Member: Can see cars, can see car sizes

- Admin: Can do CRUD about car stuff

- Superadmin: Can do CRUD about car stuff and admin stuff

Technologies used: Express, Node.js, PostgreSQL, Swagger UI

There are three services in this project:

- cars -> handle all CRUD stuff about cars stuff

- users -> handle all CRUD stuff about users stuff

- gateways -> handle loadbalancer of all services

For starter user, there is superadmin with credentials:

email: superadmin@gmail.com

password: superadmin123

For documentation api, the link is:
http://127.0.0.1:5000/api-docs

## How to run
To run, we need three terminals.

First terminal
```bash
cd /backend/cars
npm install
npm run create-database
npm run migrate
npm run seed
npm start 
```

Second terminal
```bash
cd /backend/users
npm install
npm run create-database
npm run migrate
npm run seed
npm start 
```

Third terminal
```bash
cd /backend/gateways
npm install
npm start 
```

### Endpoints REST API

GET /api/v1/users -> Getting all users

POST /api/v1/users/login -> Logins user

GET /api/v1/users/logout -> Logout user

GET /api/v1/users/profile -> Get current user description

GET /api/v1/users/admins -> Get all admins

POST /api/v1/users/admins -> Create new admin

GET /api/v1/users/admins/:id -> Get a specific admin by ID

PUT /api/v1/users/admins/:id -> Update a specific admin by ID

DELETE /api/v1/users/admins/:id -> Delete a specific admin by ID

GET /api/v1/users/members -> Get all members

POST /api/v1/users/members -> Create new member

GET /api/v1/users/members/:id -> Get a specific member by ID

PUT /api/v1/users/members/:id -> Update a specific member by ID

DELETE /api/v1/users/members/:id -> Delete a specific member by ID

GET /api/v1/cars -> Getting all cars

GET /api/v1/cars/:id -> Getting a specific car by id

POST /api/v1/cars -> Creating a car

PUT /api/v1/cars -> Update a car

DELETE /api/v1/cars/:id -> Deleting a specific car by id

GET /api/v1/cars/size -> Getting all available car size

## Directory Structure

```
.
├── backend
│   ├── cars
│   │   ├── api
│   │   │   └── v1
│   │   │       ├── cars.js
│   │   │       └── size.js
│   │   ├── controller
│   │   │   ├── cars.controller.js
│   │   │   └── size.controller.js
│   │   ├── database
│   │   │   ├── config
│   │   │   │   └── config.json
│   │   │   ├── migrations
│   │   │   │   ├── 20220417115219-create-size.js
│   │   │   │   ├── 20220417115258-create-cars.js
│   │   │   │   └── 20220424104833-create-cars-v2.js
│   │   │   ├── models
│   │   │   │   ├── cars.js
│   │   │   │   ├── index.js
│   │   │   │   └── size.js
│   │   │   └── seeders
│   │   │       ├── 20220417094652-seed-size.js
│   │   │       └── 20220424111432-seed-cars.js
│   │   ├── middleware
│   │   │   └── authenticate.js
│   │   ├── repository
│   │   │   ├── cars.repository.js
│   │   │   └── size.repository.js
│   │   ├── server
│   │   │   └── index.js
│   │   ├── services
│   │   │   ├── cars.services.js
│   │   │   └── size.services.js
│   │   ├── utils
│   │   │   ├── errors.js
│   │   │   ├── fetch.js
│   │   │   └── password.js
│   │   ├── .sequelizerc
│   │   └── package.json
│   ├── gateways
│   │   ├── index.js
│   │   ├── package.json
│   │   └── swagger.yaml
│   └── users
│       ├── api
│       │   └── v1
│       │       └── users.js
│       ├── controller
│       │   └── users.controller.js
│       ├── database
│       │   ├── config
│       │   │   └── config.json
│       │   ├── migrations
│       │   │   ├── 20220424104831-create-role
│       │   │   └── 20220424104832-create-user.js
│       │   ├── models
│       │   │   ├── index.js
│       │   │   ├── role.js
│       │   │   └── user.js
│       │   └── seeders
│       │       ├── 20220424111425-seed-role.js
│       │       └── 20220424111432-seed-user.js
│       ├── middleware
│       │   └── authenticate.js
│       ├── repository
│       │   ├── roles.repository.js
│       │   └── users.repository.js
│       ├── server
│       │   └── index.js
│       ├── services
│       │   ├── roles.services.js
│       │   └── users.services.js
│       ├── utils
│       │   ├── errors.js
│       │   └── password.js
│       ├── .sequelizerc
│       └── package.json
├── frontend
└── README.md
```

## ERD
![Entity Relationship Diagram](ERD_diagram.png)

