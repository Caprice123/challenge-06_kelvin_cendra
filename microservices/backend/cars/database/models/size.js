'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Size extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Size.hasMany(models.cars, { 
        as: 'Cars',
        foreignKey: 'size_code' 
      })
    }
  }
  Size.init({
    code: { type: DataTypes.CHAR, allowNull: false, primaryKey: true, },
    size_name: { type: DataTypes.STRING, allowNull: false },
    created_at: {allowNull: false,type: DataTypes.DATE},
    updated_at: {allowNull: false,type: DataTypes.DATE}
  }, {
    sequelize,
    modelName: 'size',
    timestamps: false,
    underscored: true
  });
  return Size;
};