const ERR_400 = "Bad Request"
const ERR_401 = "Not Authenticated"
const ERR_403 = "Forbidden"
const ERR_404 = "Not Found"
const ERR_409 = "Conflict Resources"
const ERR_500 = "Internal Server Error"

class ApplicationError extends Error{
    constructor(message, status){
        super()

        Error.captureStackTrace(this, this.constructor)
        this.message = message
        this.status = status
    }
}

class BadRequest extends ApplicationError{
    constructor(){
        super(ERR_400, 400)
    }
}

class NotAuthenticated extends ApplicationError{
    constructor(){
        super(ERR_401, 401)
    }
}

class Forbidden extends ApplicationError{
    constructor(){
        super(ERR_403, 403)
    }
}
class NotFound extends ApplicationError{
    constructor(){
        super(ERR_404, 404)
    }
}

class ResourceConflict extends ApplicationError{
    constructor(){
        super(ERR_409, 409)
    }
}

class InternalServerError extends ApplicationError{
    constructor(){
        super(ERR_500, 500)
    }
}

module.exports = { 
    ERR_400, 
    ERR_401, 
    ERR_403,
    ERR_404, 
    ERR_409, 
    ERR_500, 
    BadRequest, 
    NotAuthenticated, 
    Forbidden,
    NotFound, 
    ResourceConflict, 
    InternalServerError 
}
