const SizeService = require('../services/size.services')

class SizeApiController {

    static getSizes = async (req, res, next) => {
        try{
            // console.log("h")
            const results = await SizeService.getAllSize()
                
            res.status(200).json({ success: true, results })
        } catch(err){
            next(err)
        }
    }
}


module.exports = SizeApiController