const { Router } = require('express')
const SizeApiController = require('../../controller/size.controller.js')
const router = Router()

router.get('/', SizeApiController.getSizes)

module.exports = router

