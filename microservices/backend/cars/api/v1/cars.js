const { Router } = require('express')
const CarApiController = require('../../controller/cars.controller.js')
const Authentication = require('../../middleware/authenticate')
const router = Router()

router.get('/', CarApiController.getCars)
        .get('/:id', CarApiController.getOneById)
        .post('/',[
                Authentication.tokenRequired,
                Authentication.privateAPI
        ], CarApiController.addCar)
        .put('/:id',[
                Authentication.tokenRequired,
                Authentication.privateAPI
        ], CarApiController.updateCar)
        .delete('/:id',[
                Authentication.tokenRequired,
                Authentication.privateAPI
        ], CarApiController.deleteCar)


module.exports = router

