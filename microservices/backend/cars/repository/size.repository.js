const { size: Size } = require('../database/models')
const { InternalServerError } = require('../utils/errors')

class SizeRepository{
    static getAll = async () => {
        try{
            const sizes = await Size.findAll()
            return sizes
        } catch(err){
            throw new InternalServerError()
        }
    }

    static searchOne = async (query) => {
        try{
            const { size_name } = query
    
            const size = await Size.findOne({
                where: { size_name }
            })
            return size
        } catch(err){
            throw new InternalServerError()
        }
    }
}

module.exports = SizeRepository
