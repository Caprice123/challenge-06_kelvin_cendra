const express = require('express')
const cors = require('cors')
const proxy = require('express-http-proxy')
const swaggerUi = require('swagger-ui-express')
const YAML = require('yamljs')
const swaggerDocument = YAML.load('./swagger.yaml');



const app = express()

app.use(cors())
app.use(express.json())

app.use('/api/v1/cars', proxy('http://localhost:5001/'))
app.use('/api/v1/users', proxy('http://localhost:5002/'))
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.listen(5000, () => {
    console.log('Gateway is Listening to Port 5000')
})

