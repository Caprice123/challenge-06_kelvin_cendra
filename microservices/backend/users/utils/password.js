const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const salt = 10

const dotenv = require('dotenv')
dotenv.config()

const SECRET_KEY = process.env.JWT_SECRET_KEY || 'binar123'


const encryptPassword = (password) => {
    return new Promise((resolve, reject) => {
        bcrypt.hash(password, salt, (err, encryptedPassword) => {
            if (err){
                reject(err)
                return
            }

            resolve(encryptedPassword)
        })
    })
}

const checkPassword = (encryptedPassword, password) => {
    return new Promise((resolve, reject) => {
        bcrypt.compare(password, encryptedPassword, (err, isPasswordCorrect) => {
            if (err){
                reject(err)
                return
            }

            resolve(isPasswordCorrect)
        })
    })
}

const getTokenPayload = (token) => {
    const payload = jwt.verify(token, SECRET_KEY, { maxAge: "1h" })
    return payload
}

const createToken = (payload) => {
    const token = jwt.sign(payload, SECRET_KEY, { expiresIn: "1h" })
    return token
}

module.exports = { encryptPassword, checkPassword, getTokenPayload, createToken }
