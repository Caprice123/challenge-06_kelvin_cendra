const express = require('express')
const dotenv = require('dotenv')
const cors = require('cors')
const morgan = require('morgan')
const session = require('express-session')


const UserApi = require('../api/v1/users.js')
const { ERR_400, ERR_401, ERR_403, ERR_404, ERR_409, ERR_500 } = require('../utils/errors')

dotenv.config()

const app = express()
const PORT = process.env.PORT || 5000


app.use(morgan('tiny'))
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(cors())
app.set('trust proxy', 1)
app.use(session({
    secret: "123",
    saveUninitialized: false,
    resave: false
}))

app.use('/', UserApi)


app.use((error, req, res, next) => {
    console.log(error.message)
    switch(error.message){
        case ERR_400:
            res.status(400)
            break
        case ERR_401:
            res.status(401)
            break
        case ERR_403:
            res.status(403)
            break
        case ERR_404:
            res.status(404)
            break
        case ERR_409:
            res.status(409)
            break
        case ERR_500:
        default:
            res.status(500)
            break
    }
    res.send({ success: false, results: [] })
})

app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`)
})
