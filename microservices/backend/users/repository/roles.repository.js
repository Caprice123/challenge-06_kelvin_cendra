const { role: Roles } = require('../database/models')
const { InternalServerError } = require('../utils/errors')

class RoleRepository{
    static getAll = async () => {
        try{
            const roles = await Roles.findAll()
            return roles
        } catch(err){
            throw new InternalServerError()
        }
    }

    static searchOne = async (query) => {
        try{
            const { role_name } = query
            const role = await Roles.findOne({
                where: { role_name: role_name.toLowerCase() }
            })
    
            return role
        } catch(err){
            throw new InternalServerError()
        }
    }
}

module.exports = RoleRepository