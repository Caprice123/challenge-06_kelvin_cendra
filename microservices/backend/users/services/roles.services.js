const RoleRepository = require('../repository/roles.repository')
const { InternalServerError } = require('../utils/errors')

class RoleService{
    static getAll = async () => {
        try{
            const roles = await RoleRepository.getAll()
            // console.log(roles)
            return roles
        } catch(err){
            throw new InternalServerError()
        }
    }

    static searchOne = async (query) => {
        try{
            const role = await RoleRepository.searchOne(query)
            return role
        } catch(err){
            throw new InternalServerError()
        }
    }
}

module.exports = RoleService
