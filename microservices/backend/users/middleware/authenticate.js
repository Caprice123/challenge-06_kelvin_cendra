const { NotAuthenticated, Forbidden } = require('../utils/errors')
const { getTokenPayload } = require('../utils/password') 

class Authentication{
    static tokenRequired = async (req, res, next) => {
        try{
            const bearerToken = req.headers.authorization
            const token = bearerToken.split("Bearer ")[1]
            const payload = getTokenPayload(token)
            req.user = payload
            next()

        } catch(err){
            next(new NotAuthenticated())
            // res.status(401).send({ success: false, results: [] })
        }
    }

    static privateAPI = async (req, res, next) => {
        if (!req.user){
            next(new NotAuthenticated())
        }
        const {  role } = req.user
        console.log(req.user)
        if (role === "admin" || role === "superadmin"){
            next()
            return
        } 
        next(new Forbidden())
        // res.status(401).send({ success: false, results: [] })
    
    }

    static secretAPI = async (req, res, next) => {
        const { role } = req.user
        if (role === "superadmin"){
            next()
            return
        } 
        next(new Forbidden())
        // res.status(401).send({ success: false, results: [] })
    }
}

module.exports = Authentication
