'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      User.belongsTo(models.role, {
        foreignKey: 'role_id'
      })
    }
  }
  User.init({
    username: { type: DataTypes.STRING, allowNull: false },
    email: { type: DataTypes.STRING, allowNull: false, unique: true, },
    password: { type: DataTypes.STRING, allowNull: false, },
    role_id: { type: DataTypes.INTEGER, allowNull: false },
    created_at: {allowNull: false,type: DataTypes.DATE},
    updated_at: {allowNull: false,type: DataTypes.DATE},
  }, {
    sequelize,
    modelName: 'user',
    timestamps: false,
    underscored: true,
  });
  return User;
};