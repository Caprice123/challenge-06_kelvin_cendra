const { Router } = require('express')
const UserApiController = require('../../controller/users.controller.js')
const Authentication = require('../../middleware/authenticate')
const router = Router()

router.get('/', [
                Authentication.tokenRequired,
                Authentication.privateAPI
        ], UserApiController.getAllUsers)

router.get("/admins", [
                Authentication.tokenRequired, 
                Authentication.secretAPI
        ], UserApiController.getAllAdmin)
        .get("/admins/:id", [
                Authentication.tokenRequired, 
                Authentication.secretAPI
        ], UserApiController.getAdminById)
        .post('/admins', [
                Authentication.tokenRequired, 
                Authentication.secretAPI
        ], UserApiController.addAdmin)
        .put("/admins/:id", [
                Authentication.tokenRequired, 
                Authentication.secretAPI
        ], UserApiController.updateAdmin)
        .delete('/admins/:id', [
                Authentication.tokenRequired, 
                Authentication.secretAPI
        ], UserApiController.deleteAdmin)

router.get("/members", [
                Authentication.tokenRequired, 
                Authentication.privateAPI
        ], UserApiController.getAllMember)
        .get('/members/:id', [
                Authentication.tokenRequired, 
                Authentication.privateAPI
        ], UserApiController.getMemberById)
        .post('/members', [
                Authentication.tokenRequired, 
                Authentication.privateAPI
        ], UserApiController.addMember)
        .put("/members/:id", [
                Authentication.tokenRequired, 
                Authentication.privateAPI
        ], UserApiController.updateMember)
        .delete('/members/:id', [
                Authentication.tokenRequired, 
                Authentication.privateAPI
        ], UserApiController.deleteMember)

router.get('/profile', [
                Authentication.tokenRequired
        ], UserApiController.checkUser)
        
router.post('/login', UserApiController.signIn)

router.get('/logout', [
                Authentication.tokenRequired
        ], UserApiController.logout)

module.exports = router

