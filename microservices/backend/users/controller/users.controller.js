const UserService = require('../services/users.services')

class UserController{
    static signIn = async (req, res, next) => {
        try{
            const { email, password } = req.body
            const results = await UserService.signIn(email, password)
            
            req.session.isAuthenticated = true
            req.session.user = results

            res.status(200).json({ success: true, results })
        } catch(err){
            next(err)
        }
    }
    
    static getAllUsers = async (req, res, next) => {
        try{
            const allUsers = await UserService.getAll()
            res.status(200).json({ success: true, results: allUsers })
        } catch(err){
            next(err)
        }
    }

    static logout = async (req, res, next) => {
        try{
            req.session.destroy()
            res.status(200).json({ success: true, results: [] })
        } catch(err){
            next(err)
        }
    }

    static getAllAdmin = async (req, res, next) => {
        
        try{
            const allAdmins = await UserService.filterByRole("admin")
            res.status(200).json({ success: true, results: allAdmins })
        } catch(err){
            next(err)
        }
    }

    static getAdminById = async (req, res, next) => {
        try{
            const { id } = req.params
    
            const admin = await UserService.getUserById(id, "admin")
            res.status(200).json({ success: true, results: admin })
        } catch(err){
            next(err)
        }
    }

    static addAdmin = async (req, res, next) => {
        try{
            const { username, email, password } = req.body
            const newUser = await UserService.addUser({ username, email, password, role_name: "admin" })
    
            res.status(201).json({ success: true, results: newUser })
        } catch(err){
            next(err)
        }
    }

    static updateAdmin = async (req, res, next) => {
        try{
            const { id } = req.params
            const { username, email, password } = req.body
            const updatedAdmin = await UserService.updateUser(id, { username, email, password, role_name: "admin" })
            res.status(200).json({ success: true, results: updatedAdmin })
        } catch(err){
            next(err)
        }
    }


    static deleteAdmin = async (req, res, next) => {
        try{
            const { id } = req.params
            const deletedUser = await UserService.deleteUser(id, "admin")
            console.log("here")
            res.status(204).json({ success: true, results: [] })
        } catch(err){
            next(err)
        }

    }

    static getAllMember = async (req, res, next) => {
        try{
            const allMembers = await UserService.filterByRole("member")
            res.status(200).json({ success: true, results: allMembers })
        } catch(err){
            next(err)
        }
    }

    static getMemberById = async (req, res, next) => {
        try{
            const { id } = req.params
            const member = await UserService.getUserById(id, "member")
            res.status(200).json({ success: true, results: member })
        } catch(err){
            next(err)
        }
    }

    static addMember = async (req, res, next) => {
        try{
            const { username, email, password } = req.body
            const newMember = await UserService.addUser({ username, email, password, role_name: "member" })
            res.status(201).json({ success: true, results: newMember })
        } catch(err){
            next(err)
        }
    }
    
    static updateMember = async (req, res, next) => {
        try{
            const { id } = req.params
            const { username, email, password } = req.body
            const updatedMember = await UserService.updateUser(id, { username, email, password, role_name: "member" })
            res.status(200).json({ success: true, results: updatedMember })
        } catch(err){
            next(err)
        }
    }
    
    static deleteMember = async (req, res, next) => {
        try{
            const { id } = req.params
            const deletedUser = await UserService.deleteUser(id, "member")
            res.status(204).json({ success: true, results: [] })
        } catch(err){
            next(err)
        }
    }

    static checkUser = async (req, res, next) => {
        try{
            const payloadToken = req.user
            res.status(200).json({ success: true, results: payloadToken})
        } catch(err){
            next(err)
        }
    }
}

module.exports = UserController
